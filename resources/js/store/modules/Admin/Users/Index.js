import axios from "axios";
import * as types from '../../../mutation-types'
export const state = {
  resultData        : [],
  returnArr         : {},
  editArr           : [],
  ajax_error: { message: "", errors: [] },
};

export const getters = {
  result        : state => state.resultData,
  returnData    : state  => state.returnArr,
  editData      : state  => state.editArr,
  ajax_error    : state => state.ajax_error,
};

export const mutations = {
  [types.USERS_LIST](state, payload) {
    state.resultData = payload;
  },
  [types.GET_ROLES_DATA](state, payload) {
    state.resultData = payload;
  },
  [types.AJAX_ERROR](state, payload) {
    let message = payload.response.data.message || payload.message;
    let errors = payload.response.data.errors;
    state.ajax_error = {
      message: message,
      errors: errors
    };
  },
   [types.CREATE_USER](state, payload) {
    state.returnArr = payload;
  },
  [types.EDIT_USER](state, payload) {
    state.editArr = payload;
  },
  [types.DELETE_USER](state, payload) {
    state.returnArr = payload;
  },
};

export const actions = {
  // USERS LIST
  list: ({ commit, dispatch }, payload) => {
    if (payload.page == undefined) {
      payload.page = "";
    }
    if (payload.keyword == undefined) {
      payload.keyword = "";
    }
    axios.get("/api/users/index?page=" + payload.page+"&keyword="+payload.keyword)
      .then(response => {
        commit(types.USERS_LIST, response.data);
      })
      .catch(error => {
        message = error.response.data.message || error.message;
        commit(types.AJAX_ERROR, message);
        console.log(message);
      })
      .finally(() => {
      });
  },
  //GET ADD FORM DATA
  getAddFormData: ({ commit, dispatch }, payload) => {
    axios.get("/api/users/getRolesData")
      .then(response => {
        commit(types.GET_ROLES_DATA, response.data);
      })
      .catch(error => {
        message = error.response.data.message || error.message;
        commit(types.AJAX_ERROR, message);
        console.log(message);
      })
      .finally(() => {
      });
  },

  //ADD AND EDIT FORM
   submitForm: ({ commit, dispatch }, payload) => {
    return new Promise((resolve, reject) => {
      if (payload.id == '' || payload.id == undefined) {
        var post_url = axios.post("/api/users/add", payload);;
      }else{
         var post_url = axios.post("/api/users/updateuser/" + payload.id, payload.formData);
      }
      return post_url
        .then(response => {
          commit(types.CREATE_USER, response.data);
          resolve();
        })
        .catch(error => {
          commit(types.AJAX_ERROR, error);
          reject(true);
        })
        .finally(() => {
        });
    });
  },

  // Get User Data
  getUserData: ({ commit }, payload) => {
    return new Promise((resolve, reject) => {
      axios
        .get("/api/users/getUserData/" + payload)
        .then(response => {
          commit(types.EDIT_USER, response.data);
          resolve();
        })
        .catch(error => {
          commit(types.AJAX_ERROR, error);
          reject(true);
        });
    });
  },

  //Delete User 
  deleteUser: ({ commit, dispatch }, payload) => {
    return new Promise((resolve, reject) => {
      axios .post("/api/users/deleteuser/" +payload.id)
        .then(response => {
          commit(types.DELETE_USER, response.data);
          resolve();
        })
        .catch(error => {
          commit(types.AJAX_ERROR, error);
          reject(true);
        })
        .finally(() => {
        });
    });
  },
   
};
