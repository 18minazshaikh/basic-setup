// auth.js
export const LOGOUT			 		= 'LOGOUT'
export const SAVE_TOKEN			 	= 'SAVE_TOKEN'
export const FETCH_USER			 	= 'FETCH_USER'
export const FETCH_USER_SUCCESS		= 'FETCH_USER_SUCCESS'
export const FETCH_USER_FAILURE		= 'FETCH_USER_FAILURE'
export const UPDATE_USER			= 'UPDATE_USER'

// lang.js
export const SET_LOCALE 			= 'SET_LOCALE'
export const AJAX_ERROR 			= 'AJAX_ERROR'

//ROLES 
export const ROLES_LIST 			= 'ROLES_LIST'

//USERS 
export const USERS_LIST 			= 'USERS_LIST'
export const GET_ROLES_DATA 		= 'GET_ROLES_DATA'
export const CREATE_USER  			= 'CREATE_USER'
export const EDIT_USER  			= 'EDIT_USER'
export const DELETE_USER  			= 'DELETE_USER'

//ROLES
export const CREATE_ROLE  			= 'CREATE_ROLE'
export const EDIT_ROLE  			= 'EDIT_ROLE'
export const DELETE_ROLE  			= 'DELETE_ROLE'
