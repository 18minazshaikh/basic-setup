function page (path) {
  return () => import(/* webpackChunkName: '' */ `~/pages/${path}`).then(m => m.default || m)
}
const AdminFolder = '/admin';
export default [
  { path: '/',                    name: 'welcome',             component: page('admin/welcome.vue') },
  { path: AdminFolder + '/login',               name: 'login',               component: page('admin/auth/login.vue') },
  { path: AdminFolder,        
      component: page('admin/main_container.vue'),
      children: [
         { path: '', name: 'dashboard', component: page('admin/home.vue') },
      ]
  },
  { path: AdminFolder +'/users',        
      component: page('admin/main_container.vue'),
      children: [
         { path: '', name: 'users', component: page('admin/users/index.vue') },
         { path: 'adduser', name: 'add_user', component: page('admin/users/add.vue') },
         { path: 'edituser/:id', name: 'edit_user', component: page('admin/users/edit.vue') },
         { path: 'page/:page', name: 'users_pagination', component: page('admin/users/index.vue') },
      ]
  }
]
