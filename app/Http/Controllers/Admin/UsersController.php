<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Models\UserModel;
use App\Models\Role;
use App\Models\RoleAdminModel;

class UsersController extends Controller
{
    public function __construct(UserModel $user, Role $role, RoleAdminModel $role_admin_model)
    {
      $this->UserModel  = $user;
      $this->RoleAdminModel  = $role_admin_model;

      $this->Role  = $role;
      $this->per_page   = 10;
    }

    //GET USERS LIST
    public function index(Request $request)
    { 
      $json_arr     = [];
      $name         = trim($request->get('name'));
      $email        = trim($request->get('email'));
      $keyword           =  $request->get('keyword');
            
      $result = $this->UserModel->select('*');

      if (isset($keyword) && $keyword!= "") {
          $result = $result->whereRaw("((name LIKE '%".$keyword."%') OR (email LIKE '%".$keyword."%'))");
        } 

      $result = $result->orderBy('id', 'DESC')->paginate($this->per_page)->toArray();
      if($result != ''){
        foreach ($result['data'] as $key => $value) {
          $roleName = $this->Role->where('id',$value['role_type'])->first();
          $result['data'][$key]['role_name'] = $roleName['title'];
        }
        if(!empty($result)){
          $json_arr['mainData']  = $result;  
          $json_arr['status']    = 'success';
        }else{
          $json_arr['status']    = 'error';
          $json_arr['message']    = 'No data found!';
        }
      }else{
        $json_arr['status']    = 'error';
        $json_arr['message']    = 'No data found!';
      }
      return response()->json($json_arr);
    }

    //GET ROLES DATA
    public function getRolesData()
    {
      $result = $this->Role->where('id','!=',1)->orderBy('id', 'ASC')->get();
      if(!empty($result)){
        $json_arr['mainData']  = $result;  
        $json_arr['status']    = 'success';
      }else{
        $json_arr['status']    = 'error';
        $json_arr['message']   = 'No data found!';
      }
      return response()->json($json_arr);
    } 

    //ADD USER DATA
    public function store(Request $request)
    {
      $formData   = $request->all(); 
      $validated  = $request->validate([
                                        'first_name'        => 'required',
                                        'last_name'         => 'required',
                                        'user_name'         => 'required|unique:users',
                                        'email'             => 'required|email|unique:users',
                                        'password'          => 'required|min:6|confirmed',
                                        'password_confirmation'  => 'required|min:6|same:password',
                                        // 'role_type'         => 'required',
                                      ]);

      $first_name     = isset($formData['first_name']) ?$formData['first_name']: '';
      $last_name      = isset($formData['last_name']) ?$formData['last_name']: '';
      $user_name      = isset($formData['user_name']) ?$formData['user_name']: '';
      $email          = isset($formData['email']) ?$formData['email']: '';
      $password       = isset($formData['password']) ?$formData['password']: '';
      $role_type        = 1;

      $insertArr = array(
                    'first_name'  => $first_name,
                    'last_name'   => $last_name,
                    'user_name'   => $user_name,
                    'email'       => $email,
                    'password'    => bcrypt($password),
                    'pass_word'   => $password,
                    'role_type'   => $role_type,
      );
      $result = $this->UserModel->create($insertArr);

      if(!empty($result->id)){
        RoleAdminModel::create(['role_id' => $role_type, 'user_id' => $result->id]);
      }

      if($result){
        $json_arr['status']    = 'success';
        $json_arr['message']   =  'User Data Added Succefully!';  
      }else{
        $json_arr['status']    = 'error';
        $json_arr['message']   = 'Something went wrong! Please try again.';
      }
      return response()->json($json_arr);
    }

    //GET USERS DATA
    public function getUsersData($id)
    { 
      // print_r($id);exit;
      $json_arr = [];
      if($id != ''){
        $user = $this->UserModel->where('id',$id)->first();
        if($user){
          $json_arr['status']   = 'success';
          $json_arr['result']   =  $user; 
        }else{
          $json_arr['status']   = 'error';
          $json_arr['message']   = 'No data found!'; 
        }
      }
      return response()->json($json_arr);
    }

    //UPDATE USER DATA
    public function update(Request $request,$id)
    {
      $formData   = $request->all(); 
      $validated  = $request->validate([
                                        'first_name'        => 'required',
                                        'last_name'         => 'required',
                                        'user_name'         => 'required|unique:users,user_name, '.$id,
                                        'email'             => 'required|email|unique:users,email,' . $id,
                                        'pass_word'         => 'required|min:6',
                                        // 'role_type'         => 'required',
                                      ]);

      $first_name     = isset($formData['first_name']) ?$formData['first_name']: '';
      $last_name      = isset($formData['last_name']) ?$formData['last_name']: '';
      $user_name      = isset($formData['user_name']) ?$formData['user_name']: '';
      $email          = isset($formData['email']) ?$formData['email']: '';
      $password       = isset($formData['pass_word']) ?$formData['pass_word']: '';
      $role_type        = 1;

      $res = $this->UserModel->select('*')->where('id',$id)->first();
      $updateArr = array(
                    'first_name'  => $first_name,
                    'last_name'   => $last_name,
                    'user_name'   => $user_name,
                    'email'       => $email,
                    'password'    => bcrypt($password),
                    'pass_word'   => $password,
                    'role_type'     => $role_type,
      );
      $result = $this->UserModel->where('id',$id)->update($updateArr);
      $user = RoleAdminModel::where('user_id', '=', $id)->first();
      if ($user === null) {
        RoleAdminModel::create(['role_id' => $role_type, 'user_id' => $id]);
      }else{
        $this->RoleAdminModel->where('user_id',$id)->update(['role_id' => $role_type]);
      }

      if($result){
        $json_arr['status']    = 'success';
        $json_arr['message']   =  'User Data Updated Succefully!';  
      }else{
        $json_arr['status']    = 'error';
        $json_arr['message']   = 'Something went wrong! Please try again.';
      }
      return response()->json($json_arr);
    }

    //DELEET USER DATA
    public function delete($id)
    { 
      $json_arr = [];
      if(!empty($id)){
        $result = $this->UserModel->where('id',$id)->delete();
        $json_arr['status']   = 'success';
        $json_arr['message']  = 'User data deleted successfully!';
      }else{
        $json_arr['status']    = 'error';
        $json_arr['message']   = 'Something went wrong! Please try again.';
      }
      return response()->json($json_arr);
    }
}