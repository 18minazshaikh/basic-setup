<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Role as RoleResource;
use App\Models\Role;
use App\Models\Permission;
class LeftMenusController extends Controller
{
   public function index()
    { 
       $arg = [];	
       $user  = \Auth::user();
       $roles = $user->role;
       // dd($roles);

      if(isset($roles) && !empty($roles) && sizeof($roles)>0){
        $id = $roles[0]->id;
       	$result = Role::with(['permission'])->findOrFail($id);
           foreach($result->permission as $row){
        		array_push($arg,$row['id']);
           }
       }
       // dd($arg);
       
        $parent_tabs = Permission::select('*')->where('status',1)->where('parent_id',0)->where('is_menu',1)->orderBy('order', 'ASC')->get()->toArray();
          $data=array();
          if (isset($parent_tabs)) 
          {
            foreach($parent_tabs as $key=>$row){
                $child_tabs = Permission::select('*')->where('status',1)->where('parent_id',$row['id'])->where('is_menu',1)->get()->toArray();
                if(in_array($row['id'], $arg))
                   $data[$key] = $row;

                foreach($child_tabs as $key1=> $row1)
                {
                 if(in_array($row1['id'], $arg))
                  $data[$key]['child'][$key1] = $row1;
                }
            }
          }
         return response()->json($data);
    }

    function get_icon($slug)
    { 
      switch($slug){
        case 'dashboard': { return 'fas fa-tachometer-alt'; break;} 
        case 'reservation_center': { return 'fas fa-bookmark'; break;} 
        case 'reporting': { return 'fas fa-list-alt'; break;} 
        case 'marketing_manager': { return 'fas fa-headset'; break;} 
        case 'staff_manager': { return 'fas fa-headset'; break;} 
        case 'inventory_manager': { return 'fas fa-box-open'; break;} 
        case 'import_reservations': { return 'fas fa-box-open'; break;} 
        case 'help_desk_and_tutorials': { return 'fas fa-box-open'; break;} 
      }
    }



}