<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Permission
 *
 * @package App
 * @property string $title
*/
class Permission extends Model
{
    protected $table    = "permissions";
    protected $fillable = ['title','slug','parent_id','root_name','is_menu'];

   
    public static function boot()
    {
        parent::boot();

        //Permission::observe(new \App\Observers\AdminActionsObserver);
    }

    public static function storeValidation($request)
    {
        return [
            'title' => 'max:191|required|unique:permissions',
        ];
    }

    public static function updateValidation($request)
    {
        return [
           'title' => ['max:191','required','unique:permissions,id,'.$request->get('id')],
        ];
    }
}
