<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class UserModel extends Model
{	
	protected $table     	= 'users';
    protected $fillable  	= ['first_name','last_name','email','user_name','password','pass_word','role_type','email_verified_at','remember_token','created_at','updated_at'];
}
