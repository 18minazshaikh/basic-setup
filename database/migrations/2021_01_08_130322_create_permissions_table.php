<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title',200)->nullable();
            $table->string('slug',250)->nullable();
            $table->string('root_name',250)->nullable();
            $table->integer('parent_id')->default(0);
            $table->tinyInteger('is_menu')->default(1);
            $table->string('icon',150)->nullable();
            $table->integer('order')->nullable();
            $table->integer('status')->default(1);
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });

        DB::table('permissions')->insert([
            ['title' => 'Dashboard',    'slug' => 'dashboard',  'root_name' => 'dashboard', 'icon' => 'dashboard' ],
            ['title' => 'Roles',        'slug' => 'roles',      'root_name' => 'roles',     'icon' => 'logs'],
            ['title' => 'Users',        'slug' => 'users',      'root_name' => 'users',     'icon' => 'users'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
}
