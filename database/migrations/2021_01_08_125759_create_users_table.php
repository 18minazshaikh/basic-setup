<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('first_name',250)->nullable();
            $table->string('last_name',250)->nullable();
            $table->string('user_name',250)->nullable();
            $table->string('email',250)->nullable();
            $table->string('password',250)->nullable();
            $table->string('pass_word',250)->nullable();
            $table->integer('role_type')->nullable();
            $table->string('remember_token',200)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(\DB::raw('CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP'));
        });

        // Insert some stuff
        DB::table('users')->insert(array(
            'first_name'    => 'Nb Admin',
            'user_name'     => 'nbadmin',               
            'email'         => 'nbadmin@gmail.com',
            'password'      => '$2y$10$u5EmEr6.fKhIkmeiUvfP/OsP6QwSMyTxNDkTY4RI8Y1jRnT/XXuYe',
            'pass_word'     => '123456',
            'role_type'     => 1
        ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
