<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\ForgotPasswordController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\OAuthController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\ResetPasswordController;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\Auth\VerificationController;
use App\Http\Controllers\Settings\PasswordController;
use App\Http\Controllers\Settings\ProfileController;
use App\Http\Controllers\Admin\RolesController;
use App\Http\Controllers\Admin\UsersController;
use App\Http\Controllers\Admin\LeftMenusController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', [LoginController::class, 'logout']);
    Route::get('user', [UserController::class, 'current']);
    Route::patch('settings/profile', [ProfileController::class, 'update']);
    Route::patch('settings/password', [PasswordController::class, 'update']);
    Route::group(['namespace'=>'Admin'], function () 
    {
        Route::get('left_menus', [LeftMenusController::class, 'index']);
        //ROLES ROUTES
        Route::get('roles/index', [RolesController::class, 'index']);
        Route::get('roles/get_menus/{id}', [RolesController::class, 'getMenus']);
        Route::post('roles/store_roles', [RolesController::class, 'storeRoles']);
        Route::post('roles/deleterole/{id}', [RolesController::class, 'delete']);
        Route::get('roles/fetcheditdata/{id}', [RolesController::class, 'show']);
        Route::post('roles/update_role/{id}', [RolesController::class, 'update']);

        //USERS ROUTES
        Route::get('users/index', [UsersController::class, 'index']);
        Route::get('users/getRolesData', [UsersController::class, 'getRolesData']);
        Route::post('users/add', [UsersController::class, 'store']);
        Route::post('users/updateuser/{id}', [UsersController::class, 'update']);
        Route::get('users/getUserData/{id}', [UsersController::class, 'getUsersData']);
        Route::post('users/deleteuser/{id}', [UsersController::class, 'delete']);
    });
});

Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', [LoginController::class, 'login']);
    Route::post('register', [RegisterController::class, 'register']);

    Route::post('password/email', [ForgotPasswordController::class, 'sendResetLinkEmail']);
    Route::post('password/reset', [ResetPasswordController::class, 'reset']);

    Route::post('email/verify/{user}', [VerificationController::class, 'verify'])->name('verification.verify');
    Route::post('email/resend', [VerificationController::class, 'resend']);

    Route::post('oauth/{driver}', [OAuthController::class, 'redirect']);
    Route::get('oauth/{driver}/callback', [OAuthController::class, 'handleCallback'])->name('oauth.callback');
});